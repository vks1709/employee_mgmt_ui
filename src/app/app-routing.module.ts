import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateEmployeeComponent } from './employee/create/create.component';
import { EmployeeDetailsComponent } from './employee/details/details.component';
import { EmployeesListComponent } from './employee/list/list.component';
import { CreateSkillComponent } from './skill/create/create.component';
import { SkillDetailsComponent } from './skill/details/details.component';
import { SkillsListComponent } from './skill/list/list.component';

const routes: Routes = [ 
  { path: '', redirectTo: 'employees', pathMatch: 'full' },
  { path: 'employees', component: EmployeesListComponent },
  { path: 'employee/:id/details', component: EmployeeDetailsComponent },
  { path: 'addemployee', component: CreateEmployeeComponent },  
  { path: 'skills', component: EmployeesListComponent },
  { path: 'skill/:id', component: EmployeeDetailsComponent },
  { path: 'addskill', component: CreateEmployeeComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
