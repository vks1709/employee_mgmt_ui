import { Component, OnInit } from '@angular/core';
import { Employee } from '../employee'; 
import { EmpServiceService } from 'src/app/services/emp-service.service'; 

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class EmployeesListComponent implements OnInit {
  employee: Employee[] = [];
  constructor(public employeesService: EmpServiceService) { }

  ngOnInit(): void {
    this.employeesService.getEmployees().subscribe((data: Employee[]) => {
      this.employee = data;
  });
  }
deleteEmployee(id:number) {
  this.employeesService.deleteEmployee(id).subscribe(res => {
    this.employee = this.employee.filter(item => item.empId !== id);
  });
}
}
