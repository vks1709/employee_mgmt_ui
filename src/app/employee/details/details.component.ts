import { Component, OnInit } from '@angular/core';
import { Employee } from '../employee';
import { EmpServiceService } from 'src/app/services/emp-service.service';
import { ActivatedRoute,Router } from '@angular/router';
@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class EmployeeDetailsComponent implements OnInit {
   empId:number;
   employee:Employee;
  constructor(public empservice: EmpServiceService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
    this.empId = this.route.snapshot.params['id'];
    this.empservice.getEmployee(this.empId).subscribe((data: Employee) => {
      this.employee = data;
    });
  }

}
