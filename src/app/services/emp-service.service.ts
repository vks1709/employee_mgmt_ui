import { Injectable} from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Observable,throwError } from 'rxjs';
import { catchError } from 'rxjs';
import { Employee } from '../employee/employee';

@Injectable({
  providedIn: 'root'
})
export class EmpServiceService {
private apiURL="https://localhost:44376/api/employee";
httpOptions={headers:new HttpHeaders(
  {
    'Content-Type':'application/json'
  }
  )};
  
  constructor(private httpClient : HttpClient) { }

  getEmployees():Observable<Employee[]>
  {
    return this.httpClient.get<Employee[]>(this.apiURL+'/GetAllEmployees')
    .pipe(
      catchError(this.errorHandler)
    );
  }
  getEmployee(id:number):Observable<Employee>
  {
    return this.httpClient.get<Employee>(this.apiURL+'/GetEmployeesById/'+id)
    .pipe(
      catchError(this.errorHandler)
    );
  }
  createEmployee(employee:Employee): Observable<Employee> {
    return this.httpClient.post<Employee>(this.apiURL + '/SaveEmployees/', JSON.stringify(employee), this.httpOptions)
      .pipe(
        catchError(this.errorHandler)
      );
  }

  updatePlayer(id:number, employee:Employee): Observable<Employee> {
    return this.httpClient.put<Employee>(this.apiURL + '/SaveEmployees/' + id, JSON.stringify(employee), this.httpOptions)
      .pipe(
        catchError(this.errorHandler)
      );
  }
 
  deleteEmployee(id:number) {
    return this.httpClient.delete<Employee>(this.apiURL + '/DeleteEmployee/' + id, this.httpOptions)
      .pipe(
        catchError(this.errorHandler)
      );
  }
  errorHandler(error:any) {
    let errorMessage = '';
 
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(errorMessage);
  }
}